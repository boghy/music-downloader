import urllib.request as r
import urllib.parse
import re
import sys


import concurrent.futures as futures

SEARCH_URL = "http://vibeclouds.net/searchProxy.php"


# "http://vibeclouds.net/track/292964690/432975691/Alex_Metric-Drum_Machine_feat_The_New_Sins_.mp3?dl=1";


def downloadList(names):
    #executor = futures.ThreadPoolExecutor(max_workers=100)
    failed=[]
    for name in names:
        try:
            searchAndDownload(name)
            #a = executor.submit(searchAndDownload(name))
        except Exception:
            failed.append(name)
    print(failed)

def searchAndDownload(name):
    download_song(search(name))

def search(name):
    values = {'search': name}
    data = urllib.parse.urlencode(values)
    data = data.encode('ascii')  # data should be bytes
    req = r.Request(SEARCH_URL, data)
    req.add_header('User-Agent',
                   'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36')
    resp = r.urlopen(req)
    content = resp.read()
    # re.compile("onclick=\"location\.href='(.*)'\"")
    match = re.search(b"onclick=\"location.href='(.*)'", content)
    if match is None:
        raise Exception("Song not found:" + name)
    url = match.group(1).decode("UTF-8")
    print(url)
    return url




def download_song(url):
    name = url[url.rfind("/")+1:url.rfind("?")]
    print(name)
    req = r.Request(url)
    req.add_header('Referer', 'http://vibeclouds.net/tracks/drum_machine.html')
    req.add_header('User-Agent',
                   'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36')
    resp = r.urlopen(req)
    content = resp.read()

    f = open(name, 'wb')
    f.write(content)
    f.close()
    print("Downloaded: "+name)

def downloadSongsFromFile(filename):
    with open(filename) as f:
        content = f.readlines()
    downloadList(content)

downloadSongsFromFile("playlist.txt")
