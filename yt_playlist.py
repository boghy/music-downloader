import urllib.request as r
import urllib.parse
import json
import codecs


YT_URL="https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=%s&key=%s"
YT_API_KEY="AIzaSyBPsQjYHioM-_Mpa9Y427LXxYuHkLPHxc0"

def extractListItemTitles(content):
    titles=[]
    for song in content["items"]:
        titles.append(song["snippet"]["title"])
    return titles


def getPlaylist(playlistId):
    baseUrl = YT_URL % (playlistId, YT_API_KEY)
    url = baseUrl
    titles=[]
    while url != "":
        #print(url)
        req = r.Request(url)
        resp = r.urlopen(req)
        content = json.loads(resp.read().decode("UTF-8"))
        titles+=extractListItemTitles(content)
        if "nextPageToken" in content:
            url=baseUrl + "&pageToken="+content["nextPageToken"]
        else:
            url=""
    return titles

def writeToFile(list, fileName="playlist.txt"):
    with  codecs.open(fileName, "w", 'utf-8') as f:
        for elem in list:
            f.write(elem+"\n")


writeToFile(getPlaylist("PLqeiU5hpurhOkfhi9ft-tdeT5YOPA4PCm"))

